import wx
import gui
from wx.lib.pubsub import pub
import pickle

class madFrame(gui.MadFrame):
    def __init__(self, parent):
        gui.MadFrame.__init__(self, parent)
        self.ordre = []
        self.config = []
        pub.subscribe(self.opdaterPris, "opdater")
        self.opdaterPris()

    def opdaterPris(self):
        self.filename = "config.ini"
        infile = open(self.filename, 'rb')
        self.config = pickle.load(infile)
        infile.close()
        self.txtPizzaPris.SetLabelText(self.config[0] + ",- kr.")
        self.txtBurgerPris.SetLabelText(self.config[1] + ",- kr.")
        self.txtColaPris.SetLabelText(self.config[2] + ",- kr.")

    def prisialt(self):
        prisialt =  self.pizza_choice.GetValue() * int(self.config[0])
        prisialt += self.burger_choice.GetValue() * int(self.config[1])
        prisialt += self.cola_choice.GetValue() * int(self.config[2])
        return prisialt


    def ialt_func(self, event):
        self.txtTotalPris.SetLabelText(str(self.prisialt()))


    def bestil( self, event ):

        self.ordre.append(self.nametxt.GetValue())
        self.ordre.append(self.pizza_choice.GetValue())
        self.ordre.append(self.burger_choice.GetValue())
        self.ordre.append(self.cola_choice.GetValue())
        self.ordre.append(self.prisialt())

        pub.sendMessage("Bestilling", order = self.ordre)
        self.ordre.clear()

    def slut_mad(self, event):
        self.Hide()
        pub.sendMessage("end")

class price_change(gui.PriceChange):
    def __init__(self, parent):
        gui.PriceChange.__init__(self, parent)
        self.config = []
        self.filename = "config.ini"

    def save_price(self, event):
        self.config.append(self.pizza_input.GetValue())
        self.config.append(self.burger_input.GetValue())
        self.config.append(self.cola_input.GetValue())
        outfile = open(self.filename, 'wb')
        pickle.dump(self.config, outfile)
        outfile.close()
        pub.sendMessage("opdater")
        self.Hide()

class oversigt_mad(gui.Oversigt_mad):
    def __init__(self, parent):
        gui.Oversigt_mad.__init__(self, parent)
        pub.subscribe(self.listener, "Bestilling")
        pub.subscribe(self.end, "end")
        self.total_list = []
        self.bestilling_list.InsertColumn(0, "Navn")
        self.bestilling_list.InsertColumn(1, "Pizza")
        self.bestilling_list.InsertColumn(2, "Burger")
        self.bestilling_list.InsertColumn(3, "Cola")
        self.bestilling_list.InsertColumn(4, "Pris i alt")

        self.bestilling_list.InsertItem(0, 'I alt')
        self.bestilling_list.SetItem(0, 1, '0')
        self.bestilling_list.SetItem(0, 2, '0')
        self.bestilling_list.SetItem(0, 3, '0')
        self.bestilling_list.SetItem(0, 4, '0')

        self.index = 0

    def end(self):
        self.Hide()

    def listener(self, order):
        self.total_list.append([order[0], order[1], order[2], order[3], order[4]])

        self.bestilling_list.InsertItem(self.index, str(order[0]))
        self.bestilling_list.SetItem(self.index, 1, str(order[1]))
        self.bestilling_list.SetItem(self.index, 2, str(order[2]))
        self.bestilling_list.SetItem(self.index, 3, str(order[3]))
        self.bestilling_list.SetItem(self.index, 4, str(order[4]))

        self.index += 1

        self.empty = [0,0,0,0]
        for item in self.total_list:
            self.empty[0] += item[1]
            self.empty[1] += item[2]
            self.empty[2] += item[3]
            self.empty[3] += item[4]

        self.bestilling_list.SetItem(self.index, 1, str(self.empty[0]))
        self.bestilling_list.SetItem(self.index, 2, str(self.empty[1]))
        self.bestilling_list.SetItem(self.index, 3, str(self.empty[2]))
        self.bestilling_list.SetItem(self.index, 4, str(self.empty[3]))



class mainFrame(gui.MainFrame):
    def __init__(self, parent):
        gui.MainFrame.__init__(self, parent)
        self.madFrame = madFrame(self)
        self.oversigt_mad = oversigt_mad(self)
        self.price_change = price_change(self)

    def close( self, event ):
        exit(0)

    def mad( self, event ):
        self.madFrame.Show()
        self.oversigt_mad.Show()

    def settings(self, event):
        self.price_change.Show()

app = wx.App(False)
frame = mainFrame(None)
frame.Show(True)
app.MainLoop()

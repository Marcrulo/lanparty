# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class MainFrame
###########################################################################

class MainFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"LANparty Administration", pos = wx.DefaultPosition, size = wx.Size( 585,387 ), style = wx.DEFAULT_FRAME_STYLE|wx.MAXIMIZE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		self.m_menubar2 = wx.MenuBar( 0 )
		self.m_menu1 = wx.Menu()
		self.m_menuItem1 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Open", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu1.AppendItem( self.m_menuItem1 )
		
		self.m_menuItem2 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Save", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu1.AppendItem( self.m_menuItem2 )
		
		self.m_menuItem3 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Exit", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu1.AppendItem( self.m_menuItem3 )
		
		self.m_menubar2.Append( self.m_menu1, u"File" ) 
		
		self.m_menu2 = wx.Menu()
		self.m_menuItem4 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"Mad", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu2.AppendItem( self.m_menuItem4 )
		
		self.m_menuItem5 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"Settings", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu2.AppendItem( self.m_menuItem5 )
		
		self.m_menuItem6 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"Tilmeld og betal", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu2.AppendItem( self.m_menuItem6 )
		
		self.m_menubar2.Append( self.m_menu2, u"Edit" ) 
		
		self.SetMenuBar( self.m_menubar2 )
		
		bSizer2 = wx.BoxSizer( wx.VERTICAL )
		
		
		self.SetSizer( bSizer2 )
		self.Layout()
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.Bind( wx.EVT_MENU, self.close, id = self.m_menuItem3.GetId() )
		self.Bind( wx.EVT_MENU, self.mad, id = self.m_menuItem4.GetId() )
		self.Bind( wx.EVT_MENU, self.settings, id = self.m_menuItem5.GetId() )
		self.Bind( wx.EVT_MENU, self.registration, id = self.m_menuItem6.GetId() )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def close( self, event ):
		event.Skip()
	
	def mad( self, event ):
		event.Skip()
	
	def settings( self, event ):
		event.Skip()
	
	def registration( self, event ):
		event.Skip()
	

###########################################################################
## Class MadFrame
###########################################################################

class MadFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Bestilling", pos = wx.Point( 100,100 ), size = wx.Size( 500,288 ), style = wx.DEFAULT_FRAME_STYLE|wx.STAY_ON_TOP|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer3 = wx.BoxSizer( wx.VERTICAL )
		
		self.nametxt = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 300,-1 ), 0 )
		self.nametxt.SetToolTipString( u"Navn" )
		
		bSizer3.Add( self.nametxt, 0, wx.ALL, 5 )
		
		gSizer1 = wx.GridSizer( 0, 3, 0, 0 )
		
		self.m_staticText1 = wx.StaticText( self, wx.ID_ANY, u"Pizza", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText1.Wrap( -1 )
		gSizer1.Add( self.m_staticText1, 0, wx.ALL, 5 )
		
		self.txtPizzaPris = wx.StaticText( self, wx.ID_ANY, u"45,- kr", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.txtPizzaPris.Wrap( -1 )
		gSizer1.Add( self.txtPizzaPris, 0, wx.ALL, 5 )
		
		self.pizza_choice = wx.SpinCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 0, 42, 0 )
		gSizer1.Add( self.pizza_choice, 0, wx.ALL, 5 )
		
		self.m_staticText3 = wx.StaticText( self, wx.ID_ANY, u"Burger", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText3.Wrap( -1 )
		gSizer1.Add( self.m_staticText3, 0, wx.ALL, 5 )
		
		self.txtBurgerPris = wx.StaticText( self, wx.ID_ANY, u"45,- kr.", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.txtBurgerPris.Wrap( -1 )
		gSizer1.Add( self.txtBurgerPris, 0, wx.ALL, 5 )
		
		self.burger_choice = wx.SpinCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 0, 42, 0 )
		gSizer1.Add( self.burger_choice, 0, wx.ALL, 5 )
		
		self.m_staticText5 = wx.StaticText( self, wx.ID_ANY, u"Cola", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText5.Wrap( -1 )
		gSizer1.Add( self.m_staticText5, 0, wx.ALL, 5 )
		
		self.txtColaPris = wx.StaticText( self, wx.ID_ANY, u"12,- kr.", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.txtColaPris.Wrap( -1 )
		gSizer1.Add( self.txtColaPris, 0, wx.ALL, 5 )
		
		self.cola_choice = wx.SpinCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 0, 42, 0 )
		gSizer1.Add( self.cola_choice, 0, wx.ALL, 5 )
		
		self.m_staticText7 = wx.StaticText( self, wx.ID_ANY, u"Samlet pris:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText7.Wrap( -1 )
		self.m_staticText7.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		gSizer1.Add( self.m_staticText7, 0, wx.ALL, 5 )
		
		self.txtTotalPris = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.txtTotalPris.Wrap( -1 )
		self.txtTotalPris.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		gSizer1.Add( self.txtTotalPris, 0, wx.ALL, 5 )
		
		
		bSizer3.Add( gSizer1, 0, wx.EXPAND, 5 )
		
		self.m_button1 = wx.Button( self, wx.ID_ANY, u"Bestil", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer3.Add( self.m_button1, 0, wx.ALL, 5 )
		
		self.m_button2 = wx.Button( self, wx.ID_ANY, u"Afslut", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer3.Add( self.m_button2, 0, wx.ALL, 5 )
		
		
		self.SetSizer( bSizer3 )
		self.Layout()
		
		# Connect Events
		self.pizza_choice.Bind( wx.EVT_CHAR, self.ialt_func )
		self.pizza_choice.Bind( wx.EVT_KEY_DOWN, self.ialt_func )
		self.pizza_choice.Bind( wx.EVT_KEY_UP, self.ialt_func )
		self.pizza_choice.Bind( wx.EVT_SPINCTRL, self.ialt_func )
		self.pizza_choice.Bind( wx.EVT_TEXT, self.ialt_func )
		self.pizza_choice.Bind( wx.EVT_TEXT_ENTER, self.ialt_func )
		self.burger_choice.Bind( wx.EVT_CHAR, self.ialt_func )
		self.burger_choice.Bind( wx.EVT_KEY_DOWN, self.ialt_func )
		self.burger_choice.Bind( wx.EVT_KEY_UP, self.ialt_func )
		self.burger_choice.Bind( wx.EVT_SPINCTRL, self.ialt_func )
		self.burger_choice.Bind( wx.EVT_TEXT, self.ialt_func )
		self.burger_choice.Bind( wx.EVT_TEXT_ENTER, self.ialt_func )
		self.cola_choice.Bind( wx.EVT_CHAR, self.ialt_func )
		self.cola_choice.Bind( wx.EVT_KEY_DOWN, self.ialt_func )
		self.cola_choice.Bind( wx.EVT_KEY_UP, self.ialt_func )
		self.cola_choice.Bind( wx.EVT_SPINCTRL, self.ialt_func )
		self.cola_choice.Bind( wx.EVT_TEXT, self.ialt_func )
		self.cola_choice.Bind( wx.EVT_TEXT_ENTER, self.ialt_func )
		self.m_button1.Bind( wx.EVT_BUTTON, self.bestil )
		self.m_button2.Bind( wx.EVT_BUTTON, self.slut_mad )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def ialt_func( self, event ):
		event.Skip()
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	def bestil( self, event ):
		event.Skip()
	
	def slut_mad( self, event ):
		event.Skip()
	

###########################################################################
## Class Oversigt_mad
###########################################################################

class Oversigt_mad ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Oversigt over madbestillinger", pos = wx.Point( 800,100 ), size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.STAY_ON_TOP|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer4 = wx.BoxSizer( wx.VERTICAL )
		
		self.bestilling_list = wx.ListCtrl( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LC_REPORT )
		bSizer4.Add( self.bestilling_list, 1, wx.ALL, 5 )
		
		
		self.SetSizer( bSizer4 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class PriceChange
###########################################################################

class PriceChange ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.Point( 100,800 ), size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer5 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_staticText23 = wx.StaticText( self, wx.ID_ANY, u"Price", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText23.Wrap( -1 )
		self.m_staticText23.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_APPWORKSPACE ) )
		
		bSizer5.Add( self.m_staticText23, 0, wx.ALL, 5 )
		
		gSizer3 = wx.GridSizer( 0, 2, 0, 0 )
		
		self.pizza_txt = wx.StaticText( self, wx.ID_ANY, u"Pizza", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.pizza_txt.Wrap( -1 )
		gSizer3.Add( self.pizza_txt, 0, wx.ALL, 5 )
		
		self.pizza_input = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer3.Add( self.pizza_input, 0, wx.ALL, 5 )
		
		self.burger_txt = wx.StaticText( self, wx.ID_ANY, u"Burger", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.burger_txt.Wrap( -1 )
		gSizer3.Add( self.burger_txt, 0, wx.ALL, 5 )
		
		self.burger_input = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer3.Add( self.burger_input, 0, wx.ALL, 5 )
		
		self.cola_txt = wx.StaticText( self, wx.ID_ANY, u"Cola", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.cola_txt.Wrap( -1 )
		gSizer3.Add( self.cola_txt, 0, wx.ALL, 5 )
		
		self.cola_input = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer3.Add( self.cola_input, 0, wx.ALL, 5 )
		
		self.save_pris = wx.Button( self, wx.ID_ANY, u"Gem", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer3.Add( self.save_pris, 0, wx.ALL, 5 )
		
		
		bSizer5.Add( gSizer3, 1, wx.EXPAND, 5 )
		
		bSizer8 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_staticText27 = wx.StaticText( self, wx.ID_ANY, u"Spændende tekst", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText27.Wrap( -1 )
		bSizer8.Add( self.m_staticText27, 0, wx.ALL, 5 )
		
		
		bSizer5.Add( bSizer8, 1, wx.EXPAND, 5 )
		
		
		self.SetSizer( bSizer5 )
		self.Layout()
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.save_pris.Bind( wx.EVT_BUTTON, self.save_price )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def save_price( self, event ):
		event.Skip()
	

###########################################################################
## Class Signup
###########################################################################

class Signup ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 678,370 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		gSizer3 = wx.GridSizer( 0, 2, 0, 0 )
		
		self.m_staticText15 = wx.StaticText( self, wx.ID_ANY, u"Tilmeld", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText15.Wrap( -1 )
		gSizer3.Add( self.m_staticText15, 0, wx.ALL, 5 )
		
		self.m_staticText21 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText21.Wrap( -1 )
		gSizer3.Add( self.m_staticText21, 0, wx.ALL, 5 )
		
		self.m_staticText22 = wx.StaticText( self, wx.ID_ANY, u"Fulde navn", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText22.Wrap( -1 )
		gSizer3.Add( self.m_staticText22, 0, wx.ALL, 5 )
		
		self.full_name = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer3.Add( self.full_name, 0, wx.ALL, 5 )
		
		self.m_staticText31 = wx.StaticText( self, wx.ID_ANY, u"Email-adresse", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText31.Wrap( -1 )
		gSizer3.Add( self.m_staticText31, 0, wx.ALL, 5 )
		
		self.email = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer3.Add( self.email, 0, wx.ALL, 5 )
		
		self.m_staticText24 = wx.StaticText( self, wx.ID_ANY, u"Gruppe-navn", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText24.Wrap( -1 )
		gSizer3.Add( self.m_staticText24, 0, wx.ALL, 5 )
		
		self.group_name = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer3.Add( self.group_name, 0, wx.ALL, 5 )
		
		self.m_staticText25 = wx.StaticText( self, wx.ID_ANY, u"Brugernavn", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText25.Wrap( -1 )
		gSizer3.Add( self.m_staticText25, 0, wx.ALL, 5 )
		
		self.username = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer3.Add( self.username, 0, wx.ALL, 5 )
		
		self.m_staticText26 = wx.StaticText( self, wx.ID_ANY, u"Kodeord", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText26.Wrap( -1 )
		gSizer3.Add( self.m_staticText26, 0, wx.ALL, 5 )
		
		self.password = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PASSWORD )
		gSizer3.Add( self.password, 0, wx.ALL, 5 )
		
		self.m_staticText261 = wx.StaticText( self, wx.ID_ANY, u"Udstyr", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText261.Wrap( -1 )
		gSizer3.Add( self.m_staticText261, 0, wx.ALL, 5 )
		
		m_comboBox2Choices = []
		self.m_comboBox2 = wx.ComboBox( self, wx.ID_ANY, u"Combo!", wx.DefaultPosition, wx.DefaultSize, m_comboBox2Choices, 0 )
		gSizer3.Add( self.m_comboBox2, 0, wx.ALL, 5 )
		
		self.m_button10 = wx.Button( self, wx.ID_ANY, u"Afslut UDEN AT GEMME ændringer", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer3.Add( self.m_button10, 0, wx.ALL, 5 )
		
		self.m_button11 = wx.Button( self, wx.ID_ANY, u"Afslut og GEM ændringer", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer3.Add( self.m_button11, 0, wx.ALL, 5 )
		
		
		self.SetSizer( gSizer3 )
		self.Layout()
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.m_button10.Bind( wx.EVT_BUTTON, self.no_save )
		self.m_button11.Bind( wx.EVT_BUTTON, self.yes_save )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def no_save( self, event ):
		event.Skip()
	
	def yes_save( self, event ):
		event.Skip()
	

###########################################################################
## Class Payup
###########################################################################

class Payup ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		gSizer4 = wx.GridSizer( 0, 2, 0, 0 )
		
		self.m_staticText21 = wx.StaticText( self, wx.ID_ANY, u"Betaling", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText21.Wrap( -1 )
		gSizer4.Add( self.m_staticText21, 0, wx.ALL, 5 )
		
		self.m_staticText22 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText22.Wrap( -1 )
		gSizer4.Add( self.m_staticText22, 0, wx.ALL, 5 )
		
		self.m_staticText23 = wx.StaticText( self, wx.ID_ANY, u"Kortnummer", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText23.Wrap( -1 )
		gSizer4.Add( self.m_staticText23, 0, wx.ALL, 5 )
		
		self.m_textCtrl10 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_textCtrl10, 1, wx.ALL, 5 )
		
		self.m_staticText24 = wx.StaticText( self, wx.ID_ANY, u"Udløbsdato", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText24.Wrap( -1 )
		gSizer4.Add( self.m_staticText24, 0, wx.ALL, 5 )
		
		self.m_textCtrl11 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_textCtrl11, 1, wx.ALL, 5 )
		
		self.m_staticText25 = wx.StaticText( self, wx.ID_ANY, u"Sikkerhedskode", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText25.Wrap( -1 )
		gSizer4.Add( self.m_staticText25, 0, wx.ALL, 5 )
		
		self.m_textCtrl12 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_textCtrl12, 1, wx.ALL, 5 )
		
		self.m_button8 = wx.Button( self, wx.ID_ANY, u"Annuler", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_button8, 0, wx.ALL, 5 )
		
		self.m_button9 = wx.Button( self, wx.ID_ANY, u"Afslut og betal", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_button9, 0, wx.ALL, 5 )
		
		
		self.SetSizer( gSizer4 )
		self.Layout()
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.m_textCtrl10.Bind( wx.EVT_TEXT, self.card_number )
		self.m_textCtrl11.Bind( wx.EVT_TEXT, self.exp_date )
		self.m_textCtrl12.Bind( wx.EVT_TEXT, self.sec_number )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def card_number( self, event ):
		event.Skip()
	
	def exp_date( self, event ):
		event.Skip()
	
	def sec_number( self, event ):
		event.Skip()
	

###########################################################################
## Class Overview
###########################################################################

class Overview ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		gSizer7 = wx.GridSizer( 0, 2, 0, 0 )
		
		self.m_staticText37 = wx.StaticText( self, wx.ID_ANY, u"Lan Overview", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText37.Wrap( -1 )
		gSizer7.Add( self.m_staticText37, 0, wx.ALL, 5 )
		
		self.m_staticText38 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText38.Wrap( -1 )
		gSizer7.Add( self.m_staticText38, 0, wx.ALL, 5 )
		
		self.m_staticText39 = wx.StaticText( self, wx.ID_ANY, u"Penge brugt", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText39.Wrap( -1 )
		gSizer7.Add( self.m_staticText39, 0, wx.ALL, 5 )
		
		self.m_staticText40 = wx.StaticText( self, wx.ID_ANY, u"MyLabel", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText40.Wrap( -1 )
		gSizer7.Add( self.m_staticText40, 0, wx.ALL, 5 )
		
		self.m_staticText41 = wx.StaticText( self, wx.ID_ANY, u"Vindere af turneringer", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText41.Wrap( -1 )
		gSizer7.Add( self.m_staticText41, 0, wx.ALL, 5 )
		
		self.m_staticText42 = wx.StaticText( self, wx.ID_ANY, u"MyLabel", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText42.Wrap( -1 )
		gSizer7.Add( self.m_staticText42, 0, wx.ALL, 5 )
		
		self.m_staticText43 = wx.StaticText( self, wx.ID_ANY, u"Energiforbrug", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText43.Wrap( -1 )
		gSizer7.Add( self.m_staticText43, 0, wx.ALL, 5 )
		
		self.m_staticText44 = wx.StaticText( self, wx.ID_ANY, u"MyLabel", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText44.Wrap( -1 )
		gSizer7.Add( self.m_staticText44, 0, wx.ALL, 5 )
		
		
		self.SetSizer( gSizer7 )
		self.Layout()
		
		self.Centre( wx.BOTH )
	
	def __del__( self ):
		pass
	

